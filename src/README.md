Insight H1B Coding Challenge Comments

# Note:

1) Instructions reference reading "File Structure" documents, but these 
documents can not be found anywhere.  I have found two different file
structures and I support both.

2) The instructions say that a non-standard IEEE rounding is to be performed
on the output.  I assume this is an error since it would result in 
incompatibilities with other programs.  This code instead performs IEEE 
standard rounding.

# Development Environment

The code was developed and verified on a Windows 10 machine using Visual Studio
2017 IDE.  As such, Microsoft project files can be seen and some files might
have Windows line endings.  The code has been ported to Linux (Ubuntu) and works
in that environment.

# Code Structure

Two possible file structures are supported 

The relevant fields required from the input file are:

	STATUS, LAC_CASE_SOC_NAME, and LCA_CASE_WORKLOC1_STATE, or
	CASE_STATUS, SOC_NAME, and WORKSITE_STATE
	
The first step is to determine the index of each of these fields in the
input file, and which input file format is being used.  It is assumed that 
the field order is flexible and could be different in different input files.

The main loop consists of reading each line from the input file and
parsing out the three fields of interest.  A check is made if the
status of this field is "CERTIFIED". If so, then the "state" and the
"occupation" are saved away by calling "insert_state".

"insert_state" takes as input a Vector which stores the current state 
and a new text field and a pointer to a hash table.  The format of the
vector is an array which contains a text string plus a field counting
the number of times the indicated text string has been encountered.

The hash table is to speed access into this array.  It is not required.
It is only used in order to speed access.  The optimal size of the array
is dependent upon the nature of the input, but since this is only for 
performance reasons a non-optimal size is not a major drawback.

A hash is performed on the text input and using the hash table one of
three possible results are obtained:  

	1) text string has not been encountered previously,
	2) text string exists in the vector at a particular index,
	3) the text string exists in vector, but not at a particular index.
	
In Case #3, the entire Vector is searched for the text string.  There are
ways to find the entry without doing a complete search, but the speed
improvement would be minor and hence is not done.

If the text entry has not been found before it is added.  If it has been
found before then the count is incremented.

The last step is to sort the two vectors into the desired order and then
to print the results.

	

 



