#include <cstdio>
#include <cstring>
#include <vector>
#include <algorithm>

using namespace std;

#pragma warning (disable : 4996)

// Insight Data Science Coding Challenge, November 5, 2018

// My comments about this challenge are stored in the file README.md in the same directory as this file.

// Program for finding the top states and top occupations from a raw H1B file that has
// been converted into Excel CVS file format.
// 
// Two possible CSV coding formats are supported

// optimal Hash table size is input dependant
#define HASH_TABLE_SIZE 2048

// storage structure for saving collation entries
typedef struct {
	char* text;
	int   no;      // number of entries
} state_entry;

// add a new entry to the table
//
// temp: the storage table
// text: text field to be added
// hash: the hash table for speeding access into the table
//
void insert_state(std::vector<state_entry> *temp, const char* text, int hash [HASH_TABLE_SIZE]) {
	int x, index, i;
	state_entry new_entry;
	bool found = false;

	// compute the hash
	x = 0;
	for (i = 0; i < strlen(text); i = i + 1) { // hash the input
		x += text[i];
	}
	x = x % HASH_TABLE_SIZE;

	// find the entry for this hash
	index = hash[x];
	if (index == -1) { // not found
		found = false;
	} else { // might have found
		if (strcmp((*temp)[index].text, text) == 0) {
			found = true;
		} else {
			found = false; // do a sequential search looking for item
			for (index = 0; index < temp->size(); index = index + 1) {  // find the entry
				if (strcmp((*temp)[index].text, text) == 0) {  // found
					found = true;
					break;
				}
			}
		}
	}

	if (found) { // found
		(*temp)[index].no += 1;
	} else { // not found, create a new entry
		new_entry.text = (char*) malloc(strlen(text) + 1);
		strcpy(new_entry.text, text);
		new_entry.no = 1;
		if (hash [x] == -1) hash[x] = temp->size();
		temp->push_back(new_entry);
	}
}

// for sorting table into order
bool no_comparison(const state_entry& a, const state_entry& b) {

	if (a.no == b.no) {
		return (strcmp(a.text, b.text) < 0);
	} else {
		return (a.no > b.no);
	}
}

int main(int argc, const char* argv[]) {
	FILE* h1b_file;                 // input h1b log file
	FILE* occupations_file;         // occupations output file
	FILE* state_file;               // state output file
	char temp_line[1024];
	char *temp, *temp_ptr;
	int state_index   = -1, occupation_index   = -1, status_index   = -1, token_no, status;
	int state_index_2 = -1, occupation_index_2 = -1, status_index_2 = -1;
	unsigned int max_index;
	char h1b_filename[256], occupations_filename[256], state_filename[256];
	char temp_state[256], temp_occupation[256], temp_status[256], out_string [256];
	int line_no, i, no_certified, input_format;

	// a vector for the number of states
	// a vector for the number of occupations
	std::vector<state_entry> states, occupations;

	// hash tables
	int state_hash[HASH_TABLE_SIZE], occupations_hash[HASH_TABLE_SIZE];

	status = 0;

	if (argc < 2) {
		printf("Usage: process h1b.csv occupations.txt sessionization.txt\n");
		return 1;
	}

	for (i = 0; i < HASH_TABLE_SIZE; i = i + 1) {
		state_hash[i]       = -1;
		occupations_hash[i] = -1;
	}

	if (argc > 1) {
		strcpy(h1b_filename, argv[1]);
	} else {
		strcpy(h1b_filename, "input/h1b_input.csv");
	}

	if (argc > 2) {
		strcpy(occupations_filename, argv[2]);
	} else {
		strcpy(occupations_filename, "output/top_10_occupations.txt");
	}

	if (argc > 3) {
		strcpy(state_filename, argv[3]);
	} else {
		strcpy(occupations_filename, "output/top_10_states.txt");
	}

	h1b_file = fopen(h1b_filename, "r");
	if (h1b_file == 0) {
		printf("Unable to open h1b file %s\n", h1b_filename);
		return 1;
	}

	occupations_file = fopen(occupations_filename, "w");
	if (occupations_file == 0) {
		printf("Unable to open occupations file %s\n", occupations_filename);
		return 2;
	}

	state_file = fopen(state_filename, "w");
	if (state_file == 0) {
		printf("Unable to open state file %s\n", state_filename);
		return 3;
	}

	state_index      = -1;
	occupation_index = -1;
	status_index     = -1;

	// read h1b file header
	if (fgets(temp_line, sizeof(temp_line) - 1, h1b_file) == NULL) {
		printf("input file is empty\n");
		return 5; // empty file
	}

	token_no = 0;

	temp = temp_line;
	temp_ptr = strchr(temp, ';');
	if (temp_ptr != NULL) {
		*temp_ptr = 0;
	}

	// find the indexes for the items or interest
	do {
		if (strcmp(temp, "LCA_CASE_WORKLOC1_STATE") == 0) {  // look for matches, two possible formats
			state_index = token_no;
		} else if (strcmp(temp, "LCA_CASE_SOC_NAME") == 0) {
			occupation_index = token_no;
		} else if (strcmp(temp, "STATUS") == 0) {
			status_index = token_no;
		} else if (strcmp(temp, "CASE_STATUS") == 0) {
			status_index_2 = token_no;
		} else if (strcmp(temp, "SOC_NAME") == 0) {
			occupation_index_2 = token_no;
		} else if (strcmp(temp, "WORKSITE_STATE") == 0) {
			state_index_2 = token_no;
		}

		temp = temp_ptr + 1;
		temp_ptr = strchr(temp, ';');
		if (temp_ptr != NULL) {
			*temp_ptr = 0;
		}

		token_no += 1;
	} while (temp_ptr != NULL);

	input_format = 1;
	if ((state_index == -1) || (occupation_index == -1) || (status_index == -1)) {
		if ((state_index_2 == -1) || (occupation_index_2 == -1) || (status_index_2 == -1)) { // alternative format
			printf("header missing from h1b input file\n");
			return 3;
		} else {
			state_index      = state_index_2;
			occupation_index = occupation_index_2;
			status_index     = status_index_2;
			input_format     = 2;
		}
	}

	// find maximum required index (minor speed improvement)
	max_index = state_index;
	if (occupation_index > max_index) max_index = occupation_index;
	if (status_index > max_index) max_index = status_index;

	line_no = 0;
	no_certified = 0;
	while (fgets(temp_line, sizeof(temp_line) - 1, h1b_file) != NULL) {  // run until end of file

		// guard against premature termination of an input line
		strcpy(temp_status,     "");
		strcpy(temp_occupation, "");
		strcpy(temp_state,      "");

		temp = temp_line;
		temp_ptr = strchr(temp, ';');
		if (temp_ptr != NULL) {
			*temp_ptr = 0;
		}

		token_no = 0;
		for (i = 0; i <= max_index; i = i + 1) {
			if (token_no == status_index) { // look for matches
				strcpy(temp_status, temp);
			} else if (token_no == occupation_index) {
				strcpy(temp_occupation, temp);
			} else if (token_no == state_index) {
				strcpy(temp_state, temp);
			}

			temp = temp_ptr + 1;  // next available character in input string
			if (*temp == '"') {   // skip over quotes
				temp = temp + 1;
				temp_ptr = strchr(temp, '"');  // end of string
				*temp_ptr = 0;
				temp_ptr += 1;
				*temp_ptr = 0;
			} else {
				temp_ptr = strchr(temp, ';');
				if (temp_ptr != NULL) {
					*temp_ptr = 0;
				} else {
					break; // this is not supposed to happen!
				}
			}

			token_no += 1;
		};

		// we are only interested in certified entries
		if (strcmp(temp_status, "CERTIFIED") == 0) {
			no_certified += 1;
			insert_state(&states,      temp_state,      state_hash);
			insert_state(&occupations, temp_occupation, occupations_hash);
		}

		line_no += 1;
	};

	// first sort alphabetically by number field and then by text field
	std::sort(states.begin(),      states.end(),      no_comparison);
	std::sort(occupations.begin(), occupations.end(), no_comparison);

	fprintf(state_file, "TOP_STATES;NUMBER_CERTIFIED_APPLICATIONS;PERCENTAGE\n");
	for (i = 0; i < std::min (10, (int) states.size()); i = i + 1) {  // store output in a temporary string for debugging
		sprintf(out_string, "%s;%d;%.1f%%", states[i].text, states[i].no, (states[i].no * 100.) / no_certified);
		fprintf(state_file, "%s\n", out_string);
	};

	fprintf(occupations_file, "TOP_OCCUPATIONS;NUMBER_CERTIFIED_APPLICATIONS;PERCENTAGE\n");
	for (i = 0; i < std::min (10, (int) occupations.size()); i = i + 1) {  // store output in a temporary string for debugging
		sprintf(out_string, "%s;%d;%.1f%%", occupations[i].text, occupations[i].no, (occupations[i].no * 100.) / no_certified);
		fprintf(occupations_file, "%s\n", out_string);
	};

	// not strictly necessary to deallocate
	for (i = 0; i < states.size(); i = i + 1) {
		free(states[i].text);
	}

	for (i = 0; i < occupations.size(); i = i + 1) {
		free(occupations[i].text);
	}

	return status;
}